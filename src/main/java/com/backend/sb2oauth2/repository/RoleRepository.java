package com.backend.sb2oauth2.repository;

import com.backend.sb2oauth2.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
