package com.backend.sb2oauth2.config;

import com.backend.sb2oauth2.entity.Role;
import com.backend.sb2oauth2.entity.User;
import com.backend.sb2oauth2.repository.RoleRepository;
import com.backend.sb2oauth2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();

        if (users.isEmpty()){
            this.createUser("Alexandre", "admin", passwordEncoder.encode("123456"), "ROLE_ADMIN");
            this.createUser("Alex", "alexsoueu@gmail.com", passwordEncoder.encode("123456"), "ROLE_ALUNO");

        }

    }
    public void createUser(String name, String email, String password, String role){

        Role roleObjetc = new Role();
        roleObjetc.setName(role);

        this.roleRepository.save(roleObjetc);

        User user = new User(name, email, password, Arrays.asList(roleObjetc));
        userRepository.save(user);

    }


}
