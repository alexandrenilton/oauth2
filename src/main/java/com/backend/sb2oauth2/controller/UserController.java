package com.backend.sb2oauth2.controller;

import com.backend.sb2oauth2.entity.User;
import com.backend.sb2oauth2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    /* Retorna uma consulta pageada
     * http://localhost:8080/user?page=0&size=1 ( vai retornar de 1 em 1) */
//    @Secured({"ROLE_ADMIN"})
//    @GetMapping(value = "")
//    public Page<User> list(
//            @RequestParam("page") int page,
//            @RequestParam("size") int size ) {
//        Pageable pageable = PageRequest.of(page, size);
//        return userRepository.findAll(pageable);
//    }

    @Secured({"ROLE_ADMIN", "ROLE_ALUNO"}) // roles admin e aluno terão acesso
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> list() {
        return userRepository.findAll();
    }

    @PostMapping(value = "")
    public User save( @RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping(value = "")
    public User edit(@RequestBody User user) {
        return userRepository.save(user);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        userRepository.deleteById(id);
    }

    @GetMapping(value = "/{id}")
    public Optional<User> findById(@PathVariable Long id) {
        return userRepository.findById(id);
    }



}
